#!/bin/bash

if [ $# -lt 1 ]; then
  echo "error: You must provide a commit message"
  exit 1
fi

commit_push() {
  msg="$@"

  git add .
  git commit -m "$msg"
  git push
}

commit_push "$@"
