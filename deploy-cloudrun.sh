#!/bin/bash

PROJECT_NAME="boomerblam"

gcloud run deploy $PROJECT_NAME \
  --image=gcr.io/$PROJECT_NAME/$PROJECT_NAME:latest \
  --cpu=1 \
  --max-instances=4 \
  --memory=128Mi \
  --min-instances=0 \
  --platform=managed \
  --port=8080 \
  --tag=latest \
  --timeout=300s \
  --allow-unauthenticated \
  --cpu-throttling \
  --description="$PROJECT_NAME" \
  --region=us-central1 \
  --project=$PROJECT_NAME

for i in $(gcloud run revisions list --region=us-central1 --project=$PROJECT_NAME | awk '{print $2}'); do 
  gcloud run revisions delete $i --region=us-central1 --project=$PROJECT_NAME -q
done

gcloud artifacts docker images delete us-docker.pkg.dev/$PROJECT_NAME/gcr.io/$PROJECT_NAME -q
