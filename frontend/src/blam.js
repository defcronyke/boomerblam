import Wall from "./wall";

export default class Blam extends Wall {
  constructor(app, x, y, w, h, fillColour, blamTriggerTime) {
    super(app, x, y, w, h, fillColour || 0xffffff);

	this.name = 'Blam';

    this.r = (this.w + this.h) / 4;

    this.blamTriggerTime = blamTriggerTime || 5000;
  }

  blink(walls, boomers, doomers, blamTriggerTime, fillColour) {
    const blamBlinkSpeed = 500;
    const blamLength = 4;
    const blamOffset = 1.6;
    const blamWidth = this.w * 2;
    const blamHeight = this.h * 2;

    let triggerTime = blamTriggerTime || this.blamTriggerTime;
    triggerTime = triggerTime / blamBlinkSpeed;

    window.setTimeout(() => {
      if (triggerTime <= 1) {
        this.fillColour = 0xdd3333;
        this.show(this.r * 4);

        // up
        for (let i = 0; i < blamLength; i++) {
          const pos = -(i * this.h * blamOffset);

          const blam = new Blam(
            this.app,
            this.x,
            this.y + pos,
            blamWidth,
            blamHeight,
            this.fillColour
          );
          this.blams.push(blam);

          if (!blam.collisionWithBounds() && !blam.collisionWithRects(walls)) {
            blam.show();
          }
        }

        // down
        for (let i = 0; i < blamLength; i++) {
          const pos = i * this.h * blamOffset;

          const blam = new Blam(
            this.app,
            this.x,
            this.y + pos,
            blamWidth,
            blamHeight,
            this.fillColour
          );
          this.blams.push(blam);

          if (!blam.collisionWithBounds() && !blam.collisionWithRects(walls)) {
            blam.show();
          }
        }

        // left
        for (let i = 0; i < blamLength; i++) {
          const pos = -(i * this.w * blamOffset);

          const blam = new Blam(
            this.app,
            this.x + pos,
            this.y,
            blamWidth,
            blamHeight,
            this.fillColour
          );
          this.blams.push(blam);

          if (!blam.collisionWithBounds() && !blam.collisionWithRects(walls)) {
            blam.show();
          }
        }

        // right
        for (let i = 0; i < blamLength; i++) {
          const pos = i * this.w * blamOffset;

          const blam = new Blam(
            this.app,
            this.x + pos,
            this.y,
            blamWidth,
            blamHeight,
            this.fillColour
          );
          this.blams.push(blam);

          if (!blam.collisionWithBounds() && !blam.collisionWithRects(walls)) {
            blam.show();
          }
        }
      } else if (triggerTime % 2 == 0) {
        this.fillColour = 0x000000;
      } else {
        this.fillColour = 0xffffff;
      }

      if (triggerTime > 0) {
        this.show();

        // Check if someone blew up.
        for (const i in boomers) {
          const boomer = boomers[i];

          if (boomer.collisionWithCircles(this.blams)) {
            boomer.die(walls, boomers, doomers);
          }
        }

        for (const i in doomers) {
          const doomer = doomers[i];

          if (doomer.collisionWithCircles(this.blams)) {
            doomer.die(walls, doomers, boomers);
          }
        }

        this.blink(
          walls,
          boomers,
          doomers,
          blamTriggerTime - blamBlinkSpeed,
          fillColour
        );
      } else {
        // Check if someone blew up again.
        for (const i in boomers) {
          const boomer = boomers[i];

          if (boomer.collisionWithCircles(this.blams)) {
            boomer.die(walls, boomers, doomers);
          }

		  // Cleanup unexplodedBlams.
		  let blamIdx = 0;
		  for (const j in boomer.unexplodedBlams) {
			const blam = boomer.unexplodedBlams[j];

			if (this == blam) {
				blamIdx = j;
				break;
			}
		  }

		  boomer.unexplodedBlams.splice(blamIdx, 1);
        }

        for (const i in doomers) {
          const doomer = doomers[i];

          if (doomer.collisionWithCircles(this.blams)) {
            doomer.die(walls, doomers, boomers);
          }

		  // Cleanup unexplodedBlams.
		  let blamIdx = 0;
		  for (const j in doomer.unexplodedBlams) {
			const blam = doomer.unexplodedBlams[j];

			if (this == blam) {
				blamIdx = j;
				break;
			}
		  }

		  doomer.unexplodedBlams.splice(blamIdx, 1);
        }

        // Cleanup blams.
        for (const i in this.blams) {
          const blam = this.blams[i];

          blam.destroy();
        }
        this.blams.splice(0, this.blams.length);

        this.destroy();
      }
    }, blamBlinkSpeed);
  }

  // Optionally resize by passing r.
  show(r) {
    this.r = r || this.r;

    this.graphics.beginFill(this.fillColour);
    this.graphics.lineStyle(1, 0xf0f0f0);
    this.graphics.drawCircle(0, 0, this.r);
  }

  trigger(walls, boomers, doomers, blamTriggerTime, fillColour) {
    const triggerTime = blamTriggerTime || this.blamTriggerTime;
    const colour = fillColour || this.fillColour;

    this.blink(walls, boomers, doomers, triggerTime, colour);
  }
}
