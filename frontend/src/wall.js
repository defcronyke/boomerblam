import { Graphics, GraphicsGeometry } from "@pixi/graphics";
import { Sprite } from "@pixi/sprite";

export default class Wall {
  constructor(app, x, y, w, h, fillColour) {
    this.app = app;

	this.name = 'Wall';

    this.x = x;
    this.y = y;
    this.w = w || 34;
    this.h = h || 34;

    this.blams = [];
	this.unexplodedBlams = [];

    this.fillColour = fillColour || 0xffff00;

    this.sprite = new Sprite();
    this.sprite.x = this.x;
    this.sprite.y = this.y;
    this.sprite.vx = 0;
    this.sprite.vy = 0;

    this.graphicsGeometry = new GraphicsGeometry();
    this.graphicsGeometry.w = this.w;
    this.graphicsGeometry.h = this.h;

    this.graphics = new Graphics(this.graphicsGeometry);

    this.sprite.addChild(this.graphics);
    this.app.stage.addChild(this.sprite);
  }

  show() {
    this.graphics.beginFill(this.fillColour);
    // this.graphics.lineStyle(5, 0xFF0000);
    this.graphics.drawRect(0, 0, this.w, this.h);
  }

  /* 	Check if colliding with rectangle.
		returns:
			0 - no collision
			1 - collision +/- x
			2 - collision +/- y
			3 - collision +/- corner (x && y)
	*/
  collisionWithRects(rects) {
    for (const i in rects) {
      const rect = rects[i];

      const distX = Math.abs(this.x - rect.x - rect.w / 2);
      const distY = Math.abs(this.y - rect.y - rect.h / 2);

      if (distX > rect.w / 2 + this.r) {
        continue;
      }

      if (distY > rect.h / 2 + this.r) {
        continue;
      }

      // Left/right x collision.
      if (distX <= rect.w / 2) {
        return 1;
      }

      // Up/down y collision.
      if (distY <= rect.h / 2) {
        return 2;
      }

      // Corner x && y collision.
      const dx = distX - rect.w / 2;
      const dy = distY - rect.h / 2;

      if (dx * dx + dy * dy <= this.r * this.r) {
        return 3;
      }
    }

    return 0;
  }

  collisionWithCircles(circles) {
    for (const i in circles) {
      const circle = circles[i];

      const distX = this.x - circle.x;
      const distY = this.y - circle.y;
      const distance = Math.sqrt(distX * distX + distY * distY);

      if (distance <= this.r + circle.r) {
        return 1;
      }
    }

    return 0;
  }

  /* 	Check collision with outer edges of game.
			  returns:
				  0 - no collision
				1 - x collision with left side
				2 - x collision with right side
				3 - y collision with top
				4 - y collision with bottom
	   */
  collisionWithBounds() {
    // Left x collision.
    if (this.x < this.w / 2) {
      return 1;
    }

    // Right x collision.
    if (this.x >= this.app.renderer.width - this.w / 2) {
      return 2;
    }

    // Top y collision.
    if (this.y < this.h / 2) {
      return 3;
    }

    // Bottom y collision.
    if (this.y >= this.app.renderer.height - this.h / 2) {
      return 4;
    }

    return 0;
  }

  destroy() {
    if (this.graphics._geometry) {
      this.graphics.destroy();
    }

    if (this.sprite._texture) {
      this.sprite.destroy();
    }
  }

  // Empty stub for doomers.
  aiTurn() {}
}
