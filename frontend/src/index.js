import { Application } from "@pixi/app";
import Wall from "./wall";
import Boomer from "./boomer";
import Doomer from "./doomer";
import "./style.css";

let state;

const main = async function () {
  const app = new Application({
    backgroundColor: 0x0000ff,
  });

  document.getElementById('view').appendChild(app.view);

  const wallWidth = 34;
  const wallHeight = 34;
  const rows = 8;
  const cols = 11;
  const offset = wallWidth + 7;

  const boomerWidth = wallWidth / 2.5;
  const boomerHeight = wallHeight / 2.5;

  const boomers = [];
  const player = new Boomer(
    app,
    boomerWidth / 2 + 4,
    boomerHeight / 2 + 4,
    boomerWidth,
    boomerHeight,
    0xff00ff
  );

  boomers.push(player);

  const doomers = [];
  const doomer = new Doomer(
    app,
    app.renderer.width - boomerWidth,
    app.renderer.height - boomerHeight,
    boomerWidth,
    boomerHeight,
    0x00f0ff
  );

  doomer.aiStepping = true;

  doomers.push(doomer);

  const walls = [];

  for (let row = 0; row < rows; row++) {
    for (let col = 0; col < cols; col++) {
      const wall = new Wall(
        app,
        offset + col * (wallWidth * 2),
        offset + row * (wallHeight * 2),
        wallWidth,
        wallHeight,
        0xffff00
      );

      walls.push(wall);
    }
  }

  // Display the game objects.
  for (const i in boomers) {
    const boomer = boomers[i];
    boomer.attachInput(walls, boomers, doomers);
    boomer.show(walls, boomers, doomers);
  }

  for (const i in doomers) {
    const doomer = doomers[i];
    doomer.show();
  }

  for (const i in walls) {
    const wall = walls[i];
    wall.show();
  }

  // Set the game state.
  state = play;

  app.ticker.add((delta) => gameLoop(delta, app, walls, boomers, doomers));
};

function gameLoop(delta, app, walls, boomers, doomers) {
  state(delta, app, walls, boomers, doomers);
}

function play(delta, app, walls, boomers, doomers) {
  for (const i in boomers) {
    const boomer = boomers[i];

    boomer.step(delta, app, walls, boomers, doomers);
  }

  for (const i in doomers) {
    const doomer = doomers[i];

    doomer.aiStep(delta, app, walls, boomers, doomers);
  }
}

window.addEventListener('load', async () => {
	await main();
});
