export default function attachInput(walls, boomers, doomers) {
  for (const i in boomers) {
    const boomer = boomers[i];

	// Unsubscribe any inputs that are already attached.
	for (const j in boomer.attachedInputs) {
		const attachedInputs = boomer.attachedInputs[j];
		attachedInputs.unsubscribe();
	}

    // Capture the keyboard arrow keys.
    const left = keyboard("ArrowLeft"),
      up = keyboard("ArrowUp"),
      right = keyboard("ArrowRight"),
      down = keyboard("ArrowDown"),
      spacebar = keyboard(" ");

    // Left arrow key `press` method.
    left.press = () => {
      // Change the player's velocity when the key is pressed.
      boomer.sprite.vx = -boomer.speed;
      boomer.sprite.vy = 0;
    };

    // Left arrow key `release` method.
    left.release = () => {
      // If the left arrow has been released, and the right arrow isn't down,
      // and the player isn't moving vertically:
      // Stop the player
      if (!right.isDown && boomer.sprite.vy === 0) {
        boomer.sprite.vx = 0;
      }
    };

    // Up
    up.press = () => {
      boomer.sprite.vx = 0;
      boomer.sprite.vy = -boomer.speed;
    };
    up.release = () => {
      if (!down.isDown && boomer.sprite.vx === 0) {
        boomer.sprite.vy = 0;
      }
    };

    // Right
    right.press = () => {
      boomer.sprite.vx = boomer.speed;
      boomer.sprite.vy = 0;
    };
    right.release = () => {
      if (!left.isDown && boomer.sprite.vy === 0) {
        boomer.sprite.vx = 0;
      }
    };

    // Down
    down.press = () => {
      boomer.sprite.vy = boomer.speed;
      boomer.sprite.vx = 0;
    };
    down.release = () => {
      if (!up.isDown && boomer.sprite.vx === 0) {
        boomer.sprite.vy = 0;
      }
    };

    // Spacebar
    spacebar.press = () => {
      boomer.blam(walls, boomers, doomers, 5000);
    };
    spacebar.release = () => {};

    return { left, right, up, down, spacebar };
  }

  function keyboard(value) {
    const key = {};

    key.value = value;
    key.isDown = false;
    key.isUp = true;
    key.press = undefined;
    key.release = undefined;

    // The `downHandler`.
    key.downHandler = (event) => {
      if (event.key === key.value) {
        if (key.isUp && key.press) {
          key.press();
        }
        key.isDown = true;
        key.isUp = false;
        event.preventDefault();
      }
    };

    // The `upHandler`.
    key.upHandler = (event) => {
      if (event.key === key.value) {
        if (key.isDown && key.release) {
          key.release();
        }
        key.isDown = false;
        key.isUp = true;
        event.preventDefault();
      }
    };

    // Attach event listeners
    const downListener = key.downHandler.bind(key);
    const upListener = key.upHandler.bind(key);

    window.addEventListener("keydown", downListener, false);
    window.addEventListener("keyup", upListener, false);

    // Detach event listeners.
    key.unsubscribe = () => {
      window.removeEventListener("keydown", downListener);
      window.removeEventListener("keyup", upListener);
    };

    return key;
  }
}
