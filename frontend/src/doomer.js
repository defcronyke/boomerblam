import Boomer from "./boomer";

export default class Doomer extends Boomer {
  constructor(app, x, y, w, h, fillColour) {
    super(app, x, y, w, h, fillColour || 0x00f0ff);

	this.name = 'Doomer';

    this.r = (this.w + this.h) / 4;

    this.startX = this.app.renderer.width - this.w;
    this.startY = this.app.renderer.height - this.h;

    this.speed = 2;

    this.sprite.vx = 0;
    this.sprite.vy = 0;

    this.turnTimeout = 5000;

    this.aiStepping = false;
    this.aiTargetBoomer = null;
    this.aiTargetingBoomer = false;
    this.aiFollowing = false;
    this.aiBlamming = false;
	this.aiAvoidingBlams = false;
  }

  aiStep(delta, app, walls, boomers, doomers, aiStepTime) {
	this.step(delta, app, walls, doomers, boomers);
    this.aiTarget(boomers);
    this.aiFollow(walls, boomers, doomers, aiStepTime);
    this.aiBlam(walls, boomers, doomers, 7000, 5000);
	this.aiAvoidBlams(walls, boomers, doomers);
  }

  aiTarget(boomers, aiTargetTime) {
    // Pick a boomer at random every 30 seconds.
    if (!this.aiTargetingBoomer) {
      this.aiTargetBoomer = boomers[Math.floor(Math.random() * boomers.length)];
      this.aiTargetingBoomer = true;

      window.setTimeout(() => {
        this.aiTargetingBoomer = false;
      }, aiTargetTime || 30000);
    }
  }

  aiFollow(walls, boomers, doomers, aiFollowingTime) {
    const followingTime = aiFollowingTime || 500;

    if (!this.aiFollowing && !this.aiAvoidingBlams) {
      this.aiFollowing = true;

      const decisionTime = Math.floor(Math.random() * followingTime);

      window.setTimeout(() => {
        this.aiFollowing = false;
      }, decisionTime);

      // Try to get close to target boomer.
      let dist = { x: 0, y: 0 };
      dist.x = this.aiTargetBoomer ? this.x - this.aiTargetBoomer.x : 0;
      dist.y = this.aiTargetBoomer ? this.y - this.aiTargetBoomer.y : 0;

      const xy = ["x", "y"];

      const xOrY = Math.floor(Math.random() * 2);

      this.sprite.vx = 0;
      this.sprite.vy = 0;

      // If non-zero do normal behaviour, otherwise flip direction.
      const normalFactor = 8;
      const normalBehaviour = Math.floor(Math.random() * normalFactor);

      if (dist[xy[xOrY]] > 0) {
        if (normalBehaviour) {
          this.sprite["v" + xy[xOrY]] = -this.speed;
        } else {
          this.sprite["v" + xy[xOrY]] = this.speed;
        }
      } else {
        if (normalBehaviour) {
          this.sprite["v" + xy[xOrY]] = this.speed;
        } else {
          this.sprite["v" + xy[xOrY]] = -this.speed;
        }
      }

      let doomerIdx = 0;
      for (const i in doomers) {
        const doomer = doomers[i];

        if (this == doomer) {
          doomerIdx = i;
          break;
        }
      }
      const otherDoomers = [...doomers];
      otherDoomers.splice(doomerIdx, 1);
    }
  }

  aiTurn() {
    // Chance of turning.
    const randNum = Math.floor(Math.random() * 20);

    switch (randNum) {
      case 0:
        this.sprite.vx = -this.speed;
        this.sprite.vy = 0;
        break;

      case 1:
        this.sprite.vx = this.speed;
        this.sprite.vy = 0;
        break;

      case 2:
        this.sprite.vx = 0;
        this.sprite.vy = -this.speed;
        break;

      case 3:
        this.sprite.vx = 0;
        this.sprite.vy = this.speed;
        break;
    }
  }

  aiBlam(walls, boomers, doomers, blamMaxDropTime, blamTriggerTime) {
    if (!this.aiBlamming) {
      this.aiBlamming = true;
      const maxDropTime = blamMaxDropTime || 10000;
      const blamDropTime = Math.floor(Math.random() * maxDropTime);
      const triggerTime = blamTriggerTime || 5000;

      window.setTimeout(() => {
        this.blam(walls, boomers, doomers, triggerTime);
        this.aiBlamming = false;
      }, blamDropTime);
    }
  }

  aiAvoidBlams(walls, boomers, doomers, aiAvoidingTime) {
	const avoidingTime = aiAvoidingTime || 500;

	if (!this.aiAvoidingBlams && !this.aiFollowing) {
		this.aiAvoidingBlams = true;

		const decisionTime = Math.floor(Math.random() * aiAvoidingTime);

		window.setTimeout(() => {
			this.aiAvoidingBlams = false;
		}, decisionTime);

		// Collect all blams.
		const blams = [];

		for (const i in boomers) {
			const boomer = boomers[i];

			for (const j in boomer.unexplodedBlams) {
				const blam = boomer.unexplodedBlams[j];
				blams.push(blam);
			}
		}

		for (const i in doomers) {
			const doomer = doomers[i];

			for (const j in doomer.unexplodedBlams) {
				const blam = doomer.unexplodedBlams[j];
				blams.push(blam);
			}
		}

		// Target closest blam.
		let targetBlam = null;
		const dist = {x: this.app.renderer.width, y: this.app.renderer.height};

		for (const i in blams) {
			const blam = blams[i];

			if (Math.abs((this.x - blam.x) + (this.y - blam.y)) < Math.abs(dist.x + dist.y)) {
				dist.x = this.x - blam.x;
				dist.y = this.y - blam.y;
				targetBlam = blam;
			}
		}

		const avoidDist = 100;

		if (Math.abs(dist.x) < avoidDist) {
			this.sprite.vx = -this.sprite.vx;
		}

		if (Math.abs(dist.y) < avoidDist) {
			this.sprite.vy = -this.sprite.vy;
		}
	}
  }

  // Empty stub to disable input.
  attachInput() {}
}
