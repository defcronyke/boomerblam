import Wall from "./wall";
import Blam from "./blam";
import attachInput from "./input";

export default class Boomer extends Wall {
  constructor(app, x, y, w, h, fillColour) {
    super(app, x, y, w, h, fillColour || 0xff00ff);

	this.name = 'Boomer';

    this.r = (this.w + this.h) / 4;
    this.speed = 2;

    this.startX = this.w / 2 + 4;
    this.startY = this.h / 2 + 4;

    this.attachedInputs = null;

    this.dead = false;
  }

  attachInput(walls, boomers, doomers) {
    if (this.dead) {
      return;
    }
    this.attachedInputs = attachInput(walls, boomers, doomers);
  }

  show(walls, boomers, doomers) {
    this.graphics.beginFill(this.fillColour);
    this.graphics.drawCircle(0, 0, this.r);
  }

  step(delta, app, walls, boomers, doomers) {
    if (this.dead) {
      return;
    }

    this.sprite.x += this.sprite.vx * delta;
    this.sprite.y += this.sprite.vy * delta;

    this.x = this.sprite.x;
    this.y = this.sprite.y;

    if (this.collisionWithBounds()) {
      this.sprite.x -= this.sprite.vx * delta;
      this.sprite.y -= this.sprite.vy * delta;

      this.x = this.sprite.x;
      this.y = this.sprite.y;

      this.aiTurn();
    }

    if (this.collisionWithRects(walls)) {
      this.sprite.x -= this.sprite.vx * delta;
      this.sprite.y -= this.sprite.vy * delta;

      this.x = this.sprite.x;
      this.y = this.sprite.y;

      this.aiTurn();
    }

    let thisIdx = 0;
    for (const i in boomers) {
      const boomer = boomers[i];

      if (this == boomer) {
        thisIdx = i;
        break;
      }
    }

    const otherBoomers = [...boomers];
    otherBoomers.splice(thisIdx, 1);

    if (this.collisionWithCircles(otherBoomers)) {
      this.sprite.x -= this.sprite.vx * delta;
      this.sprite.y -= this.sprite.vy * delta;

      this.x = this.sprite.x;
      this.y = this.sprite.y;

      this.aiTurn();
    }

    if (this.collisionWithCircles(doomers)) {
      this.sprite.x -= this.sprite.vx * delta;
      this.sprite.y -= this.sprite.vy * delta;

      this.x = this.sprite.x;
      this.y = this.sprite.y;

      this.aiTurn();
    }

    if (this.collisionWithCircles(this.blams)) {
      this.die(walls, boomers, doomers);
    }
  }

  blam(walls, boomers, doomers, blamTriggerTime) {
    if (this.dead) {
      return;
    }

    const triggerTime = blamTriggerTime || 5000;

    const blam = new Blam(
      this.app,
      this.x,
      this.y,
      this.w / 2,
      this.h / 2,
      0xffffff
    );

	this.unexplodedBlams.push(blam);

    blam.show();
    blam.trigger(walls, boomers, doomers, triggerTime);
  }

  die(walls, boomers, doomers) {
    this.dead = true;

    for (const i in this.attachedInputs) {
      const attachedInput = this.attachedInputs[i];
      attachedInput.unsubscribe();
    }

    this.sprite.vx = 0;
    this.sprite.vy = 0;

    this.graphics.beginFill(0x0f505f);
    this.graphics.lineStyle(1, 0x00f505);
    this.graphics.drawCircle(0, 0, this.r * 2);

    let boomerIdx = 0;
    for (const i in boomers) {
      const boomer = boomers[i];

      if (this == boomer) {
        boomerIdx = i;
        break;
      }
    }

    boomers.splice(boomerIdx, 1);

    window.setTimeout(() => {
      this.destroy();
      this.x = this.startX;
      this.y = this.startY;

      const player = new this.constructor(
        this.app,
        this.x,
        this.y,
        this.w,
        this.h
      );

      boomers.push(player);
      player.show(walls, boomers, doomers);
      player.attachInput(walls, boomers, doomers);

	  if (doomers.length > 0) {
		switch (doomers[0].name) {
			case 'Boomer':
				const boomerScore = document.getElementById('boomerScore');
				boomerScore.textContent = (parseInt(boomerScore.textContent)+1).toString();
				break;
			
			case 'Doomer':
				const doomerScore = document.getElementById('doomerScore');
				doomerScore.textContent = (parseInt(doomerScore.textContent)+1).toString();
				break;
		}
	  }
    }, 2000);
  }
}
