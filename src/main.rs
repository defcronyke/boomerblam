use warp::Filter;

#[tokio::main]
async fn main() {
    // // GET /hello/warp => 200 OK with body "Hello, warp!"
    // let hello = warp::path!("hello" / String).map(|name| format!("Hello, {}!", name));

    let dist_dir = warp::filters::fs::dir("frontend/dist");
    let index = warp::filters::fs::file("frontend/dist/index.html");

    let routes = dist_dir.or(index);

    println!("Serving at: http://0.0.0.0:8080");

    warp::serve(routes).run(([0, 0, 0, 0], 8080)).await;
}
