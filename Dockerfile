FROM boomerblam-deps:latest as builder

ADD ./src ./src
RUN cargo build --release

# -----

FROM scratch
WORKDIR /opt/boomerblam
COPY --from=builder /opt/boomerblam/target/release/boomerblam .
COPY ./frontend/dist ./frontend/dist

EXPOSE 8080
CMD ["/opt/boomerblam/boomerblam"]
