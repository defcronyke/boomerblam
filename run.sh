#!/bin/bash

current_dir="$PWD"

cleanup() {
  cd "$current_dir"
}

trap "cleanup" INT

cd frontend
npm start
cleanup
